var BASE_URL = "https://635f4b183e8f65f283b01205.mockapi.io";

export function Service() {
  this.getListProduct = function () {
    return axios({
      url: "https://635f4b183e8f65f283b01205.mockapi.io/products",
      method: "GET",
    });
  };
}
export var getProductList = () => {
  return axios({
    url: `${BASE_URL}/products`,
    method: "GET",
  });
};
export var getSingleProduct = (id) => {
  return axios({
    url: `${BASE_URL}/products/${id}`,
    method: "GET",
  });
};

export var setSingleProduct = (item) => {
  return axios({
    url: `${BASE_URL}/products`,
    method: "POST",
    data: item,
  });
};

export var delProduct = (id) => {
  return axios({
    url: `${BASE_URL}/products/${id}`,
    method: "DELETE",
  });
};

export var putProduct = (id, item) => {
  return axios({
    url: `${BASE_URL}/products/${id}`,
    method: "PUT",
    data: item,
  });
};
